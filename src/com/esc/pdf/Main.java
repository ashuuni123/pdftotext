package com.esc.pdf;

import java.io.IOException;

/**
 * 
 * @author ashutosh kumar
 *
 */

public class Main {

	public static void main(String[] args) throws IOException {

		/** The original PDF that will be parsed. */
		String source = "information.pdf";
		/** The resulting text file. */
		String destination = "informationHindi.txt";

		PDF_TEXT pdf_text = new PDF_TEXT();
		pdf_text.parsePdf(source, destination);
	}

}
